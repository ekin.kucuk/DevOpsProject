package com.example.project;

import java.util.HashMap;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.project.controllers.ProjectController;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class ProjectContollerTest {

	@InjectMocks
	private ProjectController projectController;

	@Test
	public void testCalculate() {
		
		
        HashMap requestBody = new HashMap<>();
		
		requestBody.put("first", 3.5);
		requestBody.put("second", 22.2);
		
		assertThat(projectController.calculate(requestBody)).isInstanceOf(ResponseEntity.class);
	}

}
